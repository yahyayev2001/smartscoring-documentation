# TRA Screen and HDTS device connection

- [TRA Screen and HDTS device connection](#tra-screen-and-hdts-device-connection)
  - [Purpose of `TraScreen`](#purpose-of-trascreen)
  - [How to Configure `TraScreen`?](#how-to-configure-trascreen)
    - [Endpoint to listen (Required parameter)](#endpoint-to-listen-required-parameter)
    - [HDTS Device Endpoint (Required parameter)](#hdts-device-endpoint-required-parameter)
    - [Time for Clearing Field (sec)](#time-for-clearing-field-sec)
  - [Other notes](#other-notes)
    - [Save button](#save-button)
    - [Check Health button](#check-health-button)
  - [How to Configure HDTS Device software (Qira)](#how-to-configure-hdts-device-software-qira)
  - [Conclusion](#conclusion)

## Purpose of `TraScreen`

`TraScreen` project is intended to show each landing of participants on trampoline. The project will get data from HDTS device. HDTS Device will not be connected to our computer directly. Most of the times, it will be connected to other laptop which is in front of the judge who controls it. We will obtain the data from that laptop.

## How to Configure `TraScreen`?

In order to make flawless connection, first identify your IP address and other laptop (HDTS connected to) IP address. Open the `TraScreen` project:

![TraScreen](./images/open_trascreen_settings.png)

When you run TraScreen project, you will see window pretty much similar to picture above. There
is a hidden button behind **SmartScoring** logo (logo can be different). When you click over the logo, following popup will be appeared on the screen.

![TraScreen Settings](./images/trascreen_settings.png)

There are some textbox and descriptions below them. It is self explanatory. Let's start understand first parameter.

### Endpoint to listen (Required parameter)

HDTS device will send data to preset endpoint. This is the endpoint that our laptop will listen for data (landing of participants). In order to make flawless connection between devices, set this endpoint judiciously. Follow the steps:

1. First identify your laptops ip address. Execute `ipconfig` command on CMD (terminal). Pay attention, there might have multiple IP addresses, choose the one that is currently active.

2. Never make port number 8888, there is a problem with 8888 port for HDTS device software (Qira). Choose ports other than 8888. e.g. 8182.

3. Open this endpoint to everyone for flawless connection. In order to do that, **open your terminal as an Administrator** and execute following command.

`netsh http add urlacl url=[ENDPOINT_TO_LISTEN_URL] user=everyone`

In my case above command will be following, and don't forget to replace it with your own `endpoint to listen` url.

```shell
netsh http add urlacl url=http://192.168.1.121:8182/scoring/ user=everyone
```

If you execute above command, if there is not any problem, you will see
following message on the terminal. Again, **don't forget to run CMD (terminal) as an Administrator.**

```txt
URL reservation successfully added
```

### HDTS Device Endpoint (Required parameter)

This is the endpoint of laptop that HDTS device is connected to. In order to find IP address of that laptop, execute `ipconfig` command on CMD (terminal). As you can see on the image above, IP address will be changed only, don't change the port (8080) number because it is default port number inside **Qira** software. If port number is changed in **Qira** software, then change port number in here as well. The format of this parameter will be as follow:

`http://[IP_ADDRESS]:8080`

Make sure to set it correctly.

### Time for Clearing Field (sec)

This is the parameter that program will wipe out all the points (landing of participants) after given amount of time.

## Other notes

At the end of the popup, there are buttons which are important to know.

### Save button

After you change some parameters, don't forget to click `Save` button. In order to make the application works flawlessly as before, terminate the program by pressing `ALT+F4` and rerun the program again.

### Check Health button

The button is a helper button that may assist you to know that devices can see each other. When you click the button, it will send request to laptop where the HDTS device is connected. If the button color is changed to **Green**, connection is healty, if it stays **Red** after some time, then it means, there are some problems, ask developers to look at it.

## How to Configure HDTS Device software (Qira)

Open the software of HDTS device called **Qira**.

![Qira Software](./images/qira_software.png)

In order to configure device click the `Config` tab and click the `Network` in the opened dropdown list. New pop up window will be appeared on the screen. In the popup apply configurations that shown on below image.

![Network popup configurations](./images/network_popup_config.png)

**Don't forget to do followings:**

1. Check 'Enable HTTP Server on Port'. Don't change port number until you have a reason to change. If you change the port in here, don't forget to change port number in [TraScreen settings](#hdts-device-endpoint-required-parameter).

2. Check 'Show advanced settings' checkbox.

3. Configure client, (in this case it is laptop that `TraScreen` project is up and running) write label (write whatever name you want), url will be the endpoint that `TraScreen` project will be listening to. In my case it is `http://192.168.1.121:8182/scoring/` because this is the endpoint that I configured inside [TraScreen settings](#endpoint-to-listen-required-parameter).

4. In order to check connection is healty, click `Check connection` button. If circle is turning to green, you made connection between devices successfully, if it is turning to red then read whole document again and make sure you configured everything correctly.

## Conclusion

Congratulations if you made successful connection between devices. Contact with developers when you encounter some problems or couldn't made connection between devices.

&copy; **SmartScoring Developer Community**
